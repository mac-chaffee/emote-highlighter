import re
import time
from operator import attrgetter

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.patches import Rectangle
from matplotlib.ticker import FuncFormatter


EMOTE_REGEX = re.compile('lul', re.IGNORECASE)


def has_lul(row):
    return bool(EMOTE_REGEX.search(str(row['message'])))


def timestamp_to_seconds(ts):
    h, m, s = ts.split(":")
    return int(h) * 3600 + int(m) * 60 + int(s)


def main():
    # Read chat and ignore invalid lines
    chat = pd.read_csv("../data/mar30.csv", error_bad_lines=False)

    # Convert timestamps to number of seconds
    chat['timestamp'] = pd.Series(map(timestamp_to_seconds, chat['timestamp']))

    # Add a column which says whether the message contains lul or not
    chat['has_lul'] = chat.apply(has_lul, axis=1)
    # Filter out non-lul messages
    lul_chat = chat.loc[chat['has_lul']].drop(columns=['message'])
    # Make a histogram of luls with bins that are 60 seconds wide
    bins = range(0, max(chat['timestamp']), 60)
    figure, axis = plt.subplots()
    plt.hist(lul_chat['timestamp'], bins=bins, color='#F26430', orientation='horizontal')
    plt.yticks(range(0, max(chat['timestamp']), 180))
    # Color the max 10 bars
    bars = filter(lambda x: isinstance(x, Rectangle), axis.get_children())
    bars = sorted(bars, key=attrgetter('_width'), reverse=True)
    for bar in bars[:10]:
        bar.set_color('#BA1F33')

    # Tweak some graph settings
    figure.set_figheight(10)
    plt.title("LULs used in the March 30th NLSS")

    plt.margins(x=0, y=0)
    formatter = FuncFormatter(lambda stamp, x: time.strftime('%H:%M:%S', time.gmtime(stamp)))
    axis.yaxis.set_major_formatter(formatter)
    axis.invert_yaxis()
    axis.spines['top'].set_visible(False)
    axis.spines['right'].set_visible(False)
    axis.spines['bottom'].set_visible(False)
    plt.show()


if __name__ == "__main__":
    main()
