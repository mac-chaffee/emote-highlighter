## Twitch Emote Highlighter

Work in progress. Right now, all it does is make histograms of emote usage
over time. The only important file is [/src/analyze.py](https://gitlab.com/mac-chaffee/emote-highlighter/blob/master/src/analyze.py).

Chat logs are downloaded using [PetterKraabol/Twitch-Chat-Downloader](https://github.com/PetterKraabol/Twitch-Chat-Downloader)
and a custom format:

```json
"default": {
        "comments": {
            "format": "{timestamp[relative]},{message[body]}",
            "timestamp": {
                "relative": "%X"
            }
        },
        "output": {
            "format": "{channel[name]}/{_id}.csv",
            "timestamp": {
                "absolute": "%x"
            }
        }
    },
```

